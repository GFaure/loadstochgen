import setuptools
import re

with open("README.md", "r") as fh:
    long_description = fh.read()

def get_property(prop, project):
    result = re.search(r'{}\s*=\s*[\'"]([^\'"]*)[\'"]'.format(prop), open(project + '/__init__.py').read())
    return result.group(1)

project_name = 'loadstochgen'
setuptools.setup(
    name="loadstochgen", # Replace with your own username
    version=get_property('__version__', project_name),
    author="Gaelle Faure",
    author_email="research@gaellefaure.fr",
    description="A package to generate energy load time series from historical data.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GFaure/loadstochgen",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)