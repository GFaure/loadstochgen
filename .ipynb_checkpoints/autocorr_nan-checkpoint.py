import pandas as pd
import numpy as np

def autocorr_nan(df, lags, method = 'pearson', min_periods = 1, wantFracPairs = True):
    """
    Compute the autocorrelation for df (assuming one column) for the lags 1:lags,
    excluding NA values.
    
    Parameters:
    - method : {‘pearson’, ‘kendall’, ‘spearman’} or callable
        - pearson : standard correlation coefficient
        - kendall : Kendall Tau correlation coefficient
        - spearman : Spearman rank correlation
        - callable: callable with input two 1d ndarrays
    - min_periods : int, optional
        Minimum number of observations required per pair of columns to have a valid result.       
        Currently only available for Pearson and Spearman correlation.
        
    Outputs:
    - acf : auto-correlation coefficients for 1:lags
    - fracPairs_overAll : fraction of useful pairs (no Nan values) to compute each coefficient, 
      over the whole lentgh of the data set
    - fracPairs : fraction of useful pairs (no Nan values) to compute each coefficient, 
      over the maximum number of pairs used to compute this coefficient    
    """
    
    acf = np.empty_like(np.arange(lags), dtype="float")
    fracPairs_overAll = np.empty_like(acf)
    fracPairs = np.empty_like(acf)
    
    if not isinstance(df, pd.DataFrame):
        df = pd.DataFrame(data=df)
    
    df1 = df.iloc[:,0]
    
    len_df = len(df)
    df1_isna = df1.isna()
    
    acf[0] = 1
    if wantFracPairs:
        fracPairs_overAll[0] = 1 - df.isna().sum()[0]/len_df
        fracPairs[0] = fracPairs_overAll[0]
    
    for lag in np.arange(1,lags):
        df2 = df1.shift(lag)
        # call corr function
        acf[lag] = df1.corr(df2, method=method, min_periods=min_periods)
        # compute number of useful pairs
        if wantFracPairs:
            nanPairs = (df1_isna | df2.isna())
            fracPairs_overAll[lag] = 1 - nanPairs.sum() / len_df
            fracPairs[lag] = 1 - nanPairs[lag:].sum() / (len_df - lag)
        
    return acf, fracPairs_overAll, fracPairs
    
    