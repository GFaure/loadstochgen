import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="loadstochgen", # Replace with your own username
    version="0.0.1",
    author="Gaëlle Faure",
    author_email="research@gaellefaure.fr",
    description="A package to generate energy load time series from historical data.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GFaure/loadstochgen",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)