import numpy as np
import pandas as pd
from scipy import stats
from statsmodels.tsa.stattools import acf
from statsmodels.tools.validation import (array_like, int_like, bool_like,
                                          string_like, dict_like, float_like)
from collections.abc import Iterable
# from statsmodels.compat.python import long
from collections import namedtuple

from .autocorr_nan import *

def ljungbox_nan(x, lags=None, boxpierce=False, model_df=0, period=None, return_df=None):
    x = np.asarray(x)
    nobs = x.shape[0]
    if lags is None:
        lags = np.arange(1, min((nobs // 2 - 2), 40) + 1)
    elif isinstance(lags, int):
        lags = np.arange(1, lags + 1)
    lags = np.asarray(lags)
    maxlag = max(lags)
    # normalize by nobs not (nobs-nlags)
    # SS: unbiased=False is default now
    acfx, _, _ = autocorr_nan(df=x, lags=maxlag+1, min_periods=10)
    acf2norm = acfx[1:maxlag+1]**2 / (nobs - np.arange(1,maxlag+1))
    qljungbox = nobs * (nobs+2) * np.cumsum(acf2norm)[lags-1]
    pval = stats.chi2.sf(qljungbox, lags)
    if not boxpierce:
        return qljungbox, pval
    else:
        qboxpierce = nobs * np.cumsum(acfx[1:maxlag+1]**2)[lags-1]
        pvalbp = stats.chi2.sf(qboxpierce, lags)
        return qljungbox, pval, qboxpierce, pvalbp
    
def ljungbox_nan_2(x, lags=None, boxpierce=False, model_df=0, period=None,
                   return_df=None):
    
    x = array_like(x, "x")
    period = int_like(period, "period", optional=True)
    return_df = bool_like(return_df, "return_df", optional=True)
    model_df = int_like(model_df, "model_df", optional=False)
    if period is not None and period <= 1:
        raise ValueError("period must be >= 2")
    if model_df < 0:
        raise ValueError("model_df must be >= 0")
    nobs = x.shape[0]
    if period is not None:
        lags = np.arange(1, min(nobs // 5, 2 * period) + 1, dtype=np.int)
    elif lags is None:
        # TODO: Switch to min(10, nobs//5) after 0.12
        import warnings
        warnings.warn("The default value of lags is changing.  After 0.12, "
                      "this value will become min(10, nobs//5). Directly set"
                      "lags to silence this warning.", FutureWarning)
        # Future
        # lags = np.arange(1, min(nobs // 5, 10) + 1, dtype=np.int)
        lags = np.arange(1, min((nobs // 2 - 2), 40) + 1, dtype=np.int)
    elif not isinstance(lags, Iterable):
        lags = int_like(lags, "lags")
        lags = np.arange(1, lags + 1)
    lags = array_like(lags, "lags", dtype="int")
    maxlag = lags.max()

    # normalize by nobs not (nobs-nlags)
    # SS: unbiased=False is default now
    sacf, _, _ = autocorr_nan(df=x, lags=maxlag+1, min_periods=10)
    sacf2 = sacf[1:maxlag + 1] ** 2 / (nobs - np.arange(1, maxlag + 1))
    qljungbox = nobs * (nobs + 2) * np.cumsum(sacf2)[lags - 1]
    adj_lags = lags - model_df
    pval = np.full_like(qljungbox, np.nan)
    loc = adj_lags > 0
    pval[loc] = stats.chi2.sf(qljungbox[loc], adj_lags[loc])

    if return_df is None:
        msg = ("The value returned will change to a single DataFrame after "
               "0.12 is released.  Set return_df to True to use to return a "
               "DataFrame now.  Set return_df to False to silence this "
               "warning.")
        import warnings
        warnings.warn(msg, FutureWarning)

    if not boxpierce:
        if return_df:
            return pd.DataFrame({"lb_stat": qljungbox, "lb_pvalue": pval},
                                index=lags)
        return qljungbox, pval

    qboxpierce = nobs * np.cumsum(sacf[1:maxlag + 1] ** 2)[lags - 1]
    pvalbp = np.full_like(qljungbox, np.nan)
    pvalbp[loc] = stats.chi2.sf(qboxpierce[loc], adj_lags[loc])
    if return_df:
        return pd.DataFrame({"lb_stat": qljungbox, "lb_pvalue": pval,
                             "bp_stat": qboxpierce, "bp_pvalue": pvalbp},
                            index=lags)

    return qljungbox, pval, qboxpierce, pvalbp