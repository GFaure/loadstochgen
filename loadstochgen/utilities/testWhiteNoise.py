import numpy as np
import random as rd
import statsmodels.api as sm
from scipy import stats

from .tests_nan import *

def testWhiteNoise(timeserie, windowSize = 0.1, alphaMean = 0.05, alphaVar = 0.05):
    """
    Test if timeserie can be assimilated to white noise, which means testing 3
    conditions:
     - observations must be uncorrelated
     - mean = 0
     - constant variance over the time serie
    
    Parameters:
    - windowSize : float between 0 and 1
        Fraction of data contained in each sample for testing for mean and variance
    - alphaMean : float between 0 and 1
        Alpha value for the test for mean = 0. 
    - alphaVar : float between 0 and 1
        Alpha value for the test for constant variance.
        
    Outputs:
    - mean_ljungbox_results : mean p-value obtained for the 2 test for auto-correlation
    - frac_ttest_results : fraction of samples which passed the test for mean = 0, for provided alphaMean value.
    - mean_ttest_results : mean p-value over the samples for the test for mean = 0.
    - frac_ltest_results : fraction of samples which passed the test for constant variance, for provided alphaVar value.
    - mean_ltest_results : mean p-value over the samples for the test for constant variance. 
    """
    
    # Ljung-Box statistical test for auto-correlation
    ljungbox_results = ljungbox_nan(timeserie, lags=50, boxpierce=True)
    mean_ljungbox_results = (ljungbox_results[1][0] + ljungbox_results[3][0])/2
    
    # Remove Nan values
    timeserieWithoutNan = timeserie[~np.isnan(timeserie)]

    # Choose window size and set up the period accordingly
    period = int(windowSize * len(timeserieWithoutNan))

    # Do the sampling
    samplings = []
    wholeseries = []
    remaining = timeserieWithoutNan
    for i in range(int(len(timeserieWithoutNan)/period)):
        new_sample = rd.sample(range(len(remaining)), k=period)
        samplings.append(remaining[new_sample])
        wholeseries.append(np.delete(timeserieWithoutNan, np.isin(timeserieWithoutNan, remaining[new_sample]).nonzero()[0]))
        remaining = np.delete(remaining, new_sample)
        
    # 1-sample t-test for mean = 0
    ttest_results = []
    for sample in samplings:
        ttest_results.append(stats.ttest_1samp(sample, 0.0).pvalue)
    ttest_results = np.array(ttest_results)
    frac_ttest_results = (np.array(ttest_results) > alphaMean).sum()/len(ttest_results)
    mean_ttest_results = ttest_results.mean()
        
    # Levene's test for standard deviation constant
    ltest_result = stats.levene(*(samplings[i] for i in range(len(samplings))), center='median').pvalue
        
    return mean_ljungbox_results, frac_ttest_results, mean_ttest_results, ltest_result