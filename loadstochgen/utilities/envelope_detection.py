def getEnvelope(inputSignal, intervalLength):
# Taking the absolute value

#     absoluteSignal = []
#     for sample in inputSignal:
#         absoluteSignal.append (abs (sample))

    # Peak detection
    upperEnvelope = []
    bottomEnvelope = []

    for baseIndex in range (0, len (inputSignal)):
        maximum = -1
        minimum = 1
        for lookbackIndex in range (intervalLength):
            maximum = max (inputSignal [baseIndex - lookbackIndex], maximum)
            minimum = min (inputSignal [baseIndex - lookbackIndex], minimum)
        upperEnvelope.append (maximum)
        bottomEnvelope.append (minimum)

    return upperEnvelope, bottomEnvelope