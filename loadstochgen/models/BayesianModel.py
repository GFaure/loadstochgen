from .GenericStochasticModel import *

# General purpose
import pandas as pd
import pickle as pk
import numpy as np

# Library to perform uncertainty quantification using polynomial chaos expansions and Monte Carlo methods.
# Also provide convenient tools and classes to play with distributions which will be used there.
# Link: https://chaospy.readthedocs.io/en/development/index.html
#import chaospy as cp
# Modification of SampleDist import due to a change in the name of the function in chaospy versions >= 4.0.1
try:
    from chaospy import SampleDist
except ImportError:
    from chaospy import GaussianKDE as SampleDist
from chaospy import Uniform   

# Plotting
import matplotlib.pyplot as plt
import matplotlib.pylab as pl

class BayesianModel(GenericStochasticModel):

    def __init__(self, raw_data = pd.DataFrame()):
        GenericStochasticModel.__init__(self, raw_data)
        # Create the empty arrays for the model
        self.distributions = np.empty([12,24,2], dtype=object)
        self.samplesNumber = np.zeros_like(self.distributions)

    def save_model(self, filepath):
        with open(filepath, "wb") as fp:
            pk.dump(self.distributions, fp)

    def load_model(self, filepath):
        with open(filepath, "rb") as fp:
            self.distributions = pk.load(fp)

## Function preprocess for Bayesian model
def preprocess(self, raw_data = None):
    GenericStochasticModel.preprocess(self, raw_data)

    # Add a column for the month number
    month = self.data.index.month
    self.data["month"] = month

    # Add a column for the hour in day number
    hourOfDay = self.data.index.hour
    self.data["hourOfDay"] = hourOfDay

    # Add a column for the type of day (weekday=1, weekend=0)
    dayOfWeek = self.data.index.weekday
    isWeekday = np.zeros_like(dayOfWeek)
    isWeekday[dayOfWeek<4] = 1
    self.data["isWeekday"] = isWeekday

    return self.data

BayesianModel.preprocess = preprocess

## Definition of fit function of Bayesian model
def fit(self, preprocessed_data = None, verbose = True, save = False, filepath = ''):
    if preprocessed_data is None:
        preprocessed_data = self.data

    # Compute the distributions
    for k, g in preprocessed_data.groupby(preprocessed_data.month):   # group by month and loop
        for k1, g1 in g.groupby(g.hourOfDay):       # group by hour of day and loop
            for k2, g2 in g1.groupby(g1.isWeekday): # group by type of day and loop

                if len(g2.dropna()) > 1:
                    # Fit one distribution for each month (k-1), each hour of day k1, each type of day k2
                    # using chaospy library
                    self.distributions[k-1,k1,k2] = SampleDist(g2.dropna().iloc[:,0])
                else:
                    # Assume uniform distribution if there is not enough data
                    self.distributions[k-1,k1,k2] = Uniform(g1.iloc[:,0].min(),g1.iloc[:,0].max())

                # Number of samples used for fitting each distribution
                self.samplesNumber[k-1,k1,k2] = len(g2.dropna())

    if verbose:
        # Print some information about the number of samples used for the distributions
        print("Minimum number of samples for a distribution: " + str(self.samplesNumber.min()))
        print("Maximum number of samples for a distribution: " + str(self.samplesNumber.max()))
        print("Average number of samples for a distribution: " + str(self.samplesNumber.mean()))

    # Save distributions
    if save:
        self.save_model(filepath)

    return self.distributions

BayesianModel.fit = fit

## Definition of the function to show the resulting distributions of the Bayesian model
def show_model(self, detailed = False):
    # Set the x-axis values for the evaluation of the functions (electricity use)
    use = np.linspace(0, 6, 500)
    # Set the different colors for the months
    colors = pl.cm.jet(np.linspace(0,1,12))

    # Prepare figures and subplots
    fig, ax = plt.subplots(1, 1, figsize=(15, 4)) # a global plot
    fig1, ax1 = plt.subplots(8, 6, figsize=(16, 10)) # plots to compare month, hour of day and type of day
    # One set of axis per type of day
    ax1a = ax1[:,:3].reshape(24)
    ax1b = ax1[:,3:].reshape(24)

    # For-loop to loop over the distributions array
    for i in np.arange(12):        # loop over months
        p = 0
        for j in np.arange(24):    # loop over hours of day
            for k in np.arange(2): # loop over types of day
                ax.plot(use, self.distributions[i,j,k].pdf(use))
            ax1a[p].plot(use, self.distributions[i,j,1].pdf(use), label=i+1, color=colors[i])
            ax1b[p].plot(use, self.distributions[i,j,0].pdf(use), label=i+1, color=colors[i])
            p = p+1

    # Formatting the global plot
    ax.set_xlim([0,6])
    ax.set_xlabel('Heating demand (kWh)')
    ax.set_ylabel('Probability density distribution')
    ax.legend()

    # Formatting the second figure
    # Formatting each subplot
    for p in np.arange(24):
        ax1a[p].set_xlim([0,6])
        ax1a[p].set_ylim([0,6])
        ax1a[p].axis('off')
        ax1a[p].text(2, 2, str(p)+':00', fontsize=12, bbox={'facecolor': 'white','alpha': 0.5, 'pad': 5})
        ax1b[p].set_xlim([0,6])
        ax1b[p].set_ylim([0,6])
        ax1b[p].axis('off')
        ax1b[p].text(2, 2, str(p)+':00', fontsize=12, bbox={'facecolor': 'white','alpha': 0.5, 'pad': 5})
    # Add a line between subplots for weekday and weekend
    line = plt.Line2D([0.51,0.51],[0.1,0.9], transform=fig1.transFigure, color="black")
    fig1.add_artist(line)
    # Add a global legend
    handles, labels = ax1a[0].get_legend_handles_labels()
    fig1.legend(handles, labels, loc='center right')
    # Add title and subtitles
    fig1.suptitle("Probability density functions per month",fontsize=16)
    ax1a[0].text(0,7,'Weekday',fontsize=15)
    ax1b[2].text(6,7,'Weekend',fontsize=15, horizontalalignment='right')

BayesianModel.show_model = show_model

## Definition of the sampling function for the Bayesian model
def sample(self, nb_samples, yearly_samples = True, start_date = None, end_date = None, init_value = None, negativeValues = False):
    """ Default behaviour: create yearly samples.
    Otherwise:
     - yearly_samples == True: create samples with dates from start_date to end_date
    """

    if yearly_samples:
        index_range = pd.date_range(start  ='1/1/2019',
                                    end    ='1/1/2020',
                                    freq   ="H",
                                    closed ="left")
    else:
        index_range = pd.date_range(start = start_date,
                                    end   = end_date,
                                    freq  = "H")

    ## Create the base dataFrame
    profiles = pd.DataFrame(index = index_range)
    # Add a column for the month number
    month = profiles.index.month
    profiles["month"] = month
    # Add a column for the hour in day number
    hourOfDay = profiles.index.hour
    profiles["hourOfDay"] = hourOfDay
    # Add a column for the type of day (weekday=1, weekend=0)
    dayOfWeek = profiles.index.weekday
    isWeekday = np.zeros_like(dayOfWeek)
    isWeekday[dayOfWeek<4] = 1
    profiles["isWeekday"] = isWeekday

    ## Populate with Bayesian network model
    # Create data in a table
    tab_profiles = []
    for index, time in profiles.iterrows(): # loop over the rows of the DataFrame
        # add electricity demand for all the profiles for this time, using samplig method
        if negativeValues:
            nextValues = self.distributions[time.month-1, time.hourOfDay, time.isWeekday].sample(nb_samples)
        else:
            nextValues = np.empty(nb_samples)
            for i in np.arange(nb_samples):
                k = 0 # to prevent infinite loop
                nextValues[i] = self.distributions[time.month-1, time.hourOfDay, time.isWeekday].sample(1)
                while nextValues[i] < 0 and k < 100: #loop until obtaining a positive value
                    nextValues[i] = self.distributions[time.month-1, time.hourOfDay, time.isWeekday].sample(1)
                    k = k + 1
                if k ==  100: raise Exception("It is impossible to have a non negatve value!")

        tab_profiles.append( nextValues )

    tab_profiles = np.array(tab_profiles)

    ## Create new columns
    for p in np.arange(nb_samples):
        profiles['sample'+str(p)] = tab_profiles[:,p]

    return profiles.drop(columns=['month', 'hourOfDay', 'isWeekday'])

BayesianModel.sample = sample