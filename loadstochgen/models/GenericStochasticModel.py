import pandas as pd

class GenericStochasticModel:

    def __init__(self, raw_data = pd.DataFrame()):
        self.raw_data = raw_data
        self.data = pd.DataFrame()

    def __iter__(self): # function to propose an iterator (for eexmple on distributions?)
        return self

    def __next__(self): # function to propose an iterator (for eexmple on distributions?)
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]

    def preprocess(self, raw_data = None):
        if raw_data is None:
            raw_data = self.raw_data
        self.raw_data = raw_data.copy()
        self.data = self.raw_data.copy()
        return self.data

    def fit(self, preprocessed_data = None, verbose = True, save = False, filepath = ''):
        if preprocessed_data is None:
            preprocessed_data = self.data
        print("The function does not have been defined for this class.")
        return None

    def show_model(self, detailed = False):
        print("The function does not have been defined for this class.")
        return None

    def save_model(self, filepath):
        print("The function does not have been defined for this class.")
        return None

    def load_model(self, filepath):
        print("The function does not have been defined for this class.")
        return None

    def sample(self, nb_samples, sample_type = 'year', start_date = None, end_date = None, init_value = None, negativeValues = False):
        print("The function does not have been defined for this class.")
        return None