from .GenericStochasticModel import *

import os
import re
import pickle as pk
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import random as rd
import chaospy as cp

class Rem_SimpleDistrib(GenericStochasticModel):

    def __init__(self, raw_data = pd.DataFrame()):
        GenericStochasticModel.__init__(self, raw_data)
        self.distribution = None

    def save_model(self, filepath):
        with open(filepath, "wb") as fp:
            pk.dump(self.distribution, fp)

    def load_model(self, filepath):
        with open(filepath, "rb") as fp:
            self.distribution = pk.load(fp)

## Function preprocess for STL model
def preprocess(self, raw_data = None):
    GenericStochasticModel.preprocess(self, raw_data)
    self.data = self.data.dropna()
    return self.data

Rem_SimpleDistrib.preprocess = preprocess

def fit(self, preprocessed_data = None, verbose = True, save = False, filepath = ''):

    if preprocessed_data is None:
        preprocessed_data = self.data

    self.distribution = cp.SampleDist(preprocessed_data.iloc[:,0].values)

    # Save distributions
    if save:
        self.save_model(filepath)

    return self.distribution

Rem_SimpleDistrib.fit = fit

## Definition of the function to show the resulting distributions of the Bayesian model
def show_model(self, detailed = False):

    t = np.linspace(-10, 10, 100)
    samples_pdf = self.distribution.pdf(t).round(3)
    samples_cdf = self.distribution.cdf(t).round(3)

    # Global plot
    fig, ax = plt.subplots(1, 1, figsize=(24, 5))
    ax.plot(t, samples_pdf);
    ax.set_xlabel('use (kW)')
    ax.set_ylabel('probability density function')
    fig1, ax1 = plt.subplots(1, 1, figsize=(24, 5))
    ax1.plot(t, samples_cdf);
    ax1.set_xlabel('use (kW)')
    ax1.set_ylabel('cumulative distribution function')

Rem_SimpleDistrib.show_model = show_model

## Definition of the sampling function for the Bayesian model
def sample(self, nb_samples, yearly_samples = True, start_date = None, end_date = None, init_value = None, negativeValues = False):

    """ Default behaviour: create yearly samples.
    Otherwise:
     - yearly_samples == True: create samples with dates from start_date to end_date
    """

    if yearly_samples:
        index_range = pd.date_range(start  ='1/1/2019',
                                    end    ='1/1/2020',
                                    freq   ="H",
                                    closed ="left")
    else:
        index_range = pd.date_range(start = start_date,
                                    end   = end_date,
                                    freq  = "H")

    ## Create the dataFrame
    if negativeValues:
        profiles = pd.DataFrame(index   = index_range,
                            columns = ['sample'+str(p) for p in range(nb_samples)],
                            data    = self.distribution.sample([len(index_range), nb_samples]))
    else:
        values = np.empty(len(index_range), nb_samples)
        for i in np.arange(len(index_range)):
            for j  in np.arange(nb_samples):
                k = 0 # to prevent infinite loop
                values[i,j] = self.distribution.sample(1)
                while values[i,j] < 0 and k < 100: #loop until obtaining a positive value
                    values[i,j] = self.distribution.sample(1)
                    k = k + 1
                if k ==  100: raise Exception("It is impossible to have a non negatve value!")

    return profiles

Rem_SimpleDistrib.sample = sample

class Rem_SimpleMC(GenericStochasticModel):

    def __init__(self, raw_data = pd.DataFrame(), nbBins = 30):
        GenericStochasticModel.__init__(self, raw_data)
        self.nbBins = nbBins
        self.initialUseDistrib = None
        self.previousUse_bounds = None
        self.distributions = np.empty([nbBins], dtype=object)
        self.samplesNumber = np.zeros_like(self.distributions)

    def save_model(self, filepath):
        with open(filepath, "wb") as fp:
            pk.dump([self.initialUseDistrib, self.previousUse_bounds, self.distributions], fp)

    def load_model(self, filepath):
        with open(filepath, "rb") as fp:
            self.initialUseDistrib, self.previousUse_bounds, self.distributions = pk.load(fp)

## Function preprocess for STL model
def preprocess(self, raw_data = None):
    GenericStochasticModel.preprocess(self, raw_data)

    # Add a column for the previous value
    self.data["previousUse"] = self.data.shift(periods=1)

    return self.data

Rem_SimpleMC.preprocess = preprocess

def fit(self, preprocessed_data = None, nbBins = None, verbose = True, save = False, filepath = ''):

    if preprocessed_data is None:
        preprocessed_data = self.data

    if nbBins is None:
        nbBins = self.nbBins

    # Compute the distribution to guess initial value
    # Assume first time is always a January 1st, 0:00:00
    # (Otherwise, all distributions can be learned)
    initialUseValues = preprocessed_data[(preprocessed_data.index.month == 1)
                                         & (preprocessed_data.index.hour == 0)]
    self.initialUseDistrib = cp.SampleDist(initialUseValues.dropna().iloc[:,0])

    # Slice previous use in bins (discretize)
    previous_bins, self.previousUse_bounds = pd.cut(preprocessed_data["previousUse"], bins=nbBins, retbins=True)
    # Group by previous use bins
    i = 0
    for k2, g2 in preprocessed_data.groupby(pd.cut(preprocessed_data["previousUse"], bins=nbBins)):

        if len(g2.dropna()) > 1:
            # Learn a distribution for each group
            # using chaospy library.
            self.distributions[i] = cp.SampleDist(g2.dropna().iloc[:,0])
        else:
            # Assume uniform distribution if there is not enough data
            self.distributions[i] = cp.Uniform(preprocessed_data.iloc[:,0].min(),preprocessed_data.iloc[:,0].max())

        # Number of samples used for fitting each distribution
        self.samplesNumber[i] = len(g2.dropna())
        i = i+1

    if verbose:
        # Print some information about the number of samples used for the distributions
        print("Minimum number of samples for a distribution: " + str(self.samplesNumber.min()))
        print("Maximum number of samples for a distribution: " + str(self.samplesNumber.max()))
        print("Average number of samples for a distribution: " + str(self.samplesNumber.mean()))

    return self.distributions

Rem_SimpleMC.fit = fit

## Definition of the function to show the resulting distributions of the Bayesian model
def show_model(self, detailed = False):
    print('TODO !')

Rem_SimpleMC.show_model = show_model

# Find the corresponding column in the frequencies table
def position(self, x):
    pos = np.argmax(x < self.previousUse_bounds) - 1
    if pos == -1 and (x < self.previousUse_bounds[0]):
        return 0
    elif pos == -1 and (x >= self.previousUse_bounds[-1]):
        return self.nbBins - 1
    elif pos == -1:
        print(x < self.previousUse_bounds[0])
        print(x >= self.previousUse_bounds[-1])
        print(x)
        raise Exception('Problem with position function.')
    else:
        return pos


## Definition of the sampling function for the Bayesian model
def sample(self, nb_samples, yearly_samples = True, start_date = None, end_date = None, init_value = None, negativeValues = False):

    """ Default behaviour: create yearly samples.
    Otherwise:
     - yearly_samples == True: create samples with dates from start_date to end_date
    """

    if yearly_samples:
        index_range = pd.date_range(start  ='1/1/2019',
                                        end    ='1/1/2020',
                                        freq   ="H",
                                        closed ="left")
        # Initialise previous use value
        initialUse = self.initialUseDistrib.sample(nb_samples)
    else:
        index_range = pd.date_range(start = start_date,
                                    end   = end_date,
                                    freq  = "H")
        # Initialise previous use value: defined by the user
        initialUse = init_value * np.ones(nb_samples)

    ## Create the base dataFrame
    profiles = pd.DataFrame(index = index_range)

    ## Populate with Markov chain model
    # Create data in a table
    tab_profiles = np.empty([len(profiles),nb_samples], dtype=np.float64)

    tab_profiles[0,:] = initialUse
    previousUse = initialUse
    for t in range(1,len(profiles)): # loop over the rows of the DataFrame
#        if t%1000 == 0: print(t);

        # add electricity demand for all the profiles for this time, using samplig method
        col = np.array([position(self, use) for use in previousUse])

        # Use the cumulative frequency function to derive the use value
        # Obtain the proper distributions for each profile
        distribs = self.distributions[col]
        # Apply each distribution to each seed value
        for i, f in enumerate(distribs):
            value = f.sample(1)

            if not negativeValues:
                k = 0 # to prevent infinite loop
                while value < 0 and k < 100: #loop until obtaining a positive value
                    value = f.sample(1)
                    k = k + 1
                if k ==  100: raise Exception("It is impossible to have a non negatve value!")

            tab_profiles[t,i] = value

        # Update the previousUse
        previousUse = tab_profiles[t,:]

    ## Create new columns
    for p in np.arange(nb_samples):
        profiles['sample'+str(p)] = tab_profiles[:,p]

    return profiles

Rem_SimpleMC.sample = sample