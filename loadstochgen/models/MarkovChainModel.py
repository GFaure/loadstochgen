from .GenericStochasticModel import *

# General purpose
#import os
#import math
import pandas as pd
import pickle as pk
import numpy as np
#import datetime

# Library to perform uncertainty quantification using polynomial chaos expansions and Monte Carlo methods.
# Also provide convenient tools and classes to play with distributions which will be used there.
# Link: https://chaospy.readthedocs.io/en/development/index.html
import chaospy as cp
#import random

# Fourier transform
#import scipy.fftpack as fft
#from scipy import interpolate

# Plotting
#import matplotlib.pyplot as plt
#import matplotlib.pylab as pl
#import matplotlib.dates as mdates

class MarkovChainModel(GenericStochasticModel):

    def __init__(self, raw_data = pd.DataFrame(), nbBins = 10):
        GenericStochasticModel.__init__(self, raw_data)
        # Create the empty arrays for the model
        self.nbBins = nbBins
        self.initialUseDistrib = None
        self.previousUse_bounds = np.empty([12,24], dtype=object)
        self.distributions = np.empty([12,24,nbBins], dtype=object)
        self.samplesNumber = np.zeros_like(self.distributions)

    def set_nbBins(self,nbBins):
        print("Warning: this action will reset the model. You will have to re-fit it if you already did it.")
        answer = None
        while answer not in ("yes", "no"):
            answer = input("Do you confirm your action? (yes/no) ")
            if answer == "yes":
                self.nbBins = nbBins
                self.distributions = np.empty([12,24,nbBins], dtype=object)
                self.samplesNumber = np.zeros_like(self.distributions)
            elif answer == "no":
                print("Action aborted.")
                return
            else:
                print("Please enter yes or no.")

    def save_model(self, filepath):
        with open(filepath, "wb") as fp:
            pk.dump([self.initialUseDistrib, self.previousUse_bounds, self.distributions], fp)

    def load_model(self, filepath):
        with open(filepath, "rb") as fp:
            self.initialUseDistrib, self.previousUse_bounds, self.distributions = pk.load(fp)

## Function preprocess for Markov chain model
def preprocess(self, raw_data = None):
    GenericStochasticModel.preprocess(self, raw_data)

    # Add a column for the previous value
    self.data["previousUse"] = self.data.shift(periods=1)

    # Add a column for the month number
    month = self.data.index.month
    self.data["month"] = month

    # Add a column for the hour in day number
    hourOfDay = self.data.index.hour
    self.data["hourOfDay"] = hourOfDay

    return self.data

MarkovChainModel.preprocess = preprocess

## Definition of fit function of Markov chain model
def fit(self, preprocessed_data = None, nbBins = None, verbose = True, save = False, filepath = ''):
    if preprocessed_data is None:
        preprocessed_data = self.data

    if nbBins is None:
        nbBins = self.nbBins

    # Compute the distribution to guess initial value
    # Assume first time is always a January 1st, 0:00:00
    # (Otherwise, all distributions can be learned)
    initialUseValues = preprocessed_data[(preprocessed_data.month == 1) & (preprocessed_data.hourOfDay == 0)]
    self.initialUseDistrib = cp.SampleDist(initialUseValues.dropna().iloc[:,0])

    # Compute the distributions to iterate
    for k, g in preprocessed_data.groupby(preprocessed_data.month):   # group by month and loop
        for k1, g1 in g.groupby(g.hourOfDay):       # group by hour of day and loop

            # Slice previous use in bins (discretize)
            previous_bins, self.previousUse_bounds[k-1,k1] = pd.cut(g1["previousUse"].dropna(), bins=nbBins, retbins=True)
            # Group by previous use bins
            i = 0

            for k2, g2 in g1.groupby(pd.cut(g1["previousUse"], bins=nbBins)):

#                 if (len(g2.dropna()) > 2) or (len(g2.dropna()) > 1 and g2.dropna().iloc[0,0] != g2.dropna().iloc[1,0]) :
                if len(g2.dropna()) > 1:
                    # Learn a distribution for each group
                    # using chaospy library.
                    self.distributions[k-1,k1,i] = cp.SampleDist(g2.dropna().iloc[:,0])

                    if np.isnan(self.distributions[k-1,k1,i].sample(1)):
                        self.distributions[k-1,k1,i] = cp.Uniform(g2.iloc[:,0].min(),g2.iloc[:,0].max())

                else:
                    # Assume uniform distribution if there is not enough data
                    self.distributions[k-1,k1,i] = cp.Uniform(g1.iloc[:,0].min(),g1.iloc[:,0].max())

                # Number of samples used for fitting each distribution
                self.samplesNumber[k-1,k1,i] = len(g2.dropna())
                i = i+1

    if verbose:
        # Print some information about the number of samples used for the distributions
        print("Minimum number of samples for a distribution: " + str(self.samplesNumber.min()))
        print("Maximum number of samples for a distribution: " + str(self.samplesNumber.max()))
        print("Average number of samples for a distribution: " + str(self.samplesNumber.mean()))

    # Save distributions
    if save:
        self.save_model(filepath)

    return self.distributions

MarkovChainModel.fit = fit

## Definition of the function to show the resulting distributions of the Markov Chain model
def show_model(self, detailed = False):
    print('TODO !')

MarkovChainModel.show_model = show_model

## Definition of the sampling function for the Bayesian model
def sample(self, nb_samples, yearly_samples = True, start_date = None, end_date = None, init_value = None, negativeValues = False):
    """ Default behaviour: create yearly samples.
    Otherwise:
     - yearly_samples == True: create samples with dates from start_date to end_date, starting with init_value
    """


    if yearly_samples:
        index_range = pd.date_range(start  = '1/1/2019',
                                        end    = '1/1/2020',
                                        freq   = "H",
                                        closed = "left")
        # Initialise previous use value
        initialUse = self.initialUseDistrib.sample(nb_samples)
    else:
        index_range = pd.date_range(start = start_date,
                                    end   = end_date,
                                    freq  = "H")
        # Initialise previous use value: defined by the user
        initialUse = init_value * np.ones(nb_samples)

    ## Create the base dataFrame
    profiles = pd.DataFrame(index = index_range)
    # Add a column for the month number
    month = profiles.index.month
    profiles["month"] = month
    # Add a column for the hour in day number
    hourOfDay = profiles.index.hour
    profiles["hourOfDay"] = hourOfDay

    ## Populate with Markov chain model
    # Create data in a table
    tab_profiles = np.empty([len(profiles),nb_samples], dtype=np.float64)
    initialized = False
    newUse = np.empty([nb_samples], dtype=np.float64)
    t = 1
    for index, time in profiles.iterrows(): # loop over the rows of the DataFrame

        # first iteration: put the initial use value on first line
        if not initialized:
            tab_profiles[0,:] = initialUse
            previousUse = initialUse
            initialized = True
        else:
            #print(previousUse)

            # add electricity demand for all the profiles for this time, using samplig method

            # Find the corresponding column in the frequencies table
            def position(x):
                pos = np.argmax(x < self.previousUse_bounds[time.month-1, time.hourOfDay]) - 1
                if pos == -1 and (x < self.previousUse_bounds[time.month-1, time.hourOfDay][0]):
                    return 0
                elif pos == -1 and (x >= self.previousUse_bounds[time.month-1, time.hourOfDay][-1]):
                    return self.nbBins - 1
                elif pos == -1:
                    print(x < self.previousUse_bounds[time.month-1, time.hourOfDay][0])
                    print(x >= self.previousUse_bounds[time.month-1, time.hourOfDay][-1])
                    print(x)
                    raise Exception('Problem with position function.')
                else:
                    return pos

            col = np.array([position(use) for use in previousUse])
            #print(col)

            # Use the cumulative frequency function to derive the use value
            # Obtain the proper distributions for each profile
            distribs = self.distributions[time.month-1, time.hourOfDay, col]
            # Apply each distribution to each seed value
            for i, f in enumerate(distribs):
                value = f.sample(1)

                if not negativeValues:
                    k = 0 # to prevent infinite loop
                    while value < 0 and k < 100: #loop until obtaining a positive value
                        value = f.sample(1)
                        k = k + 1
                    if k ==  100: raise Exception("It is impossible to have a non negatve value!")

                tab_profiles[t,i] = value
                if np.isnan(value):
                    print(f)
                    print(time.month-1)
                    print(time.hourOfDay)
                    print(col[i])

            # Update the previousUse
            previousUse = tab_profiles[t,:]

            t = t+1

    ## Create new columns
    for p in np.arange(nb_samples):
        profiles['sample'+str(p)] = tab_profiles[:,p]

    return profiles.drop(columns=['month', 'hourOfDay'])

MarkovChainModel.sample = sample