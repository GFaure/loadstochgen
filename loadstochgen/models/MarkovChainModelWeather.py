from .GenericStochasticModel import *

# General purpose
#import os
#import math
import pandas as pd
import pickle as pk
import numpy as np
#import datetime

# Library to perform uncertainty quantification using polynomial chaos expansions and Monte Carlo methods.
# Also provide convenient tools and classes to play with distributions which will be used there.
# Link: https://chaospy.readthedocs.io/en/development/index.html
import chaospy as cp
#import random

from datetime import timedelta

# Fourier transform
#import scipy.fftpack as fft
#from scipy import interpolate

# Plotting
#import matplotlib.pyplot as plt
#import matplotlib.pylab as pl
#import matplotlib.dates as mdates

class MarkovChainModelWeather(GenericStochasticModel):

    def __init__(self, raw_data = pd.DataFrame(), nbBinsUse = 10, nbBinsExog = 25):
        GenericStochasticModel.__init__(self, raw_data)
        # Create the empty arrays for the model
        self.nbBinsExog = nbBinsExog
        self.nbBinsUse = nbBinsUse
        self.initialUseDistrib = None
        self.exogBins_bounds = None
        self.previousUse_bounds = np.empty([nbBinsExog], dtype=object)
        self.distributions = np.empty([nbBinsExog, nbBinsUse], dtype=object)
        self.samplesNumber = np.zeros_like(self.distributions)

    def set_nbBinsUse(self,nbBinsUse):
        print("Warning: this action will reset the model. You will have to re-fit it if you already did it.")
        answer = None
        while answer not in ("yes", "no"):
            answer = input("Do you confirm your action? (yes/no) ")
            if answer == "yes":
                self._set_nbBinsUse(nbBinsUse)
            elif answer == "no":
                print("Action aborted.")
                return
            else:
                print("Please enter yes or no.")

    def _set_nbBinsUse(self,nbBinsUse):
        self.nbBinsUse = nbBinsUse
        self.distributions = np.empty([self.nbBinsExog, nbBinsUse], dtype=object)
        self.samplesNumber = np.zeros_like(self.distributions)

    def set_nbBinsExog(self,nbBinsExog):
        print("Warning: this action will reset the model. You will have to re-fit it if you already did it.")
        answer = None
        while answer not in ("yes", "no"):
            answer = input("Do you confirm your action? (yes/no) ")
            if answer == "yes":
                self._set_nbBinsExog(nbBinsExog)
            elif answer == "no":
                print("Action aborted.")
                return
            else:
                print("Please enter yes or no.")

    def _set_nbBinsExog(self,nbBinsExog):
        self.nbBinsExog = nbBinsExog
        self.previousUse_bounds = np.empty([nbBinsExog], dtype=object)
        self.distributions = np.empty([nbBinsExog, self.nbBinsUse], dtype=object)
        self.samplesNumber = np.zeros_like(self.distributions)

    def save_model(self, filepath):
        with open(filepath, "wb") as fp:
            pk.dump([self.initialUseDistrib, self.previousUse_bounds, self.exogBins_bounds, self.distributions], fp)

    def load_model(self, filepath):
        with open(filepath, "rb") as fp:
            self.initialUseDistrib, self.previousUse_bounds, self.exogBins_bounds, self.distributions = pk.load(fp)

## Function preprocess for Markov chain model
def preprocess(self, raw_data = None):
    GenericStochasticModel.preprocess(self, raw_data)

    # Add a column for the previous value
    self.data["previousUse"] = self.data.shift(periods=1)

    return self.data

MarkovChainModelWeather.preprocess = preprocess

## Definition of fit function of Markov chain model
def fit(self, exog_data, preprocessed_data = None, nbBinsUse = None, nbBinsExog = None, verbose = True, save = False, filepath = ''):
    if preprocessed_data is None:
        preprocessed_data = self.data

    if nbBinsUse is None:
        nbBinsUse = self.nbBinsUse
    else:
        self._set_nbBinsUse(nbBinsUse)

    if nbBinsExog is None:
        nbBinsExog = self.nbBinsExog
    else:
        self._set_nbBinsExog(nbBinsExog)

    if not exog_data.index.equals(preprocessed_data.index):
        raise Exception("exog_data and preprocessed_data must have the same index.")
    preprocessed_data["exog_data"] = exog_data.iloc[:,0]

    # Compute the distribution to guess initial value
    # Assume first time is always a January 1st, 0:00:00
    # (Otherwise, all distributions can be learned)
    initialUseValues = preprocessed_data[(preprocessed_data.index.month == 1) & (preprocessed_data.index.hour == 0)]
    self.initialUseDistrib = cp.SampleDist(initialUseValues.dropna().iloc[:,0])

    # Compute the distributions to iterate
    # Slice weather data in bins (discretize)
    exog_bins, self.exogBins_bounds = pd.cut(preprocessed_data["exog_data"].dropna(), bins=nbBinsExog, retbins=True)
    i = 0

    for k, g in preprocessed_data.groupby(pd.cut(preprocessed_data["exog_data"], bins=nbBinsExog)):   # Group by weather data bins

        # Slice previous use in bins (discretize)
        previous_bins, self.previousUse_bounds[i] = pd.cut(g["previousUse"].dropna(), bins=nbBinsUse, retbins=True)
        # Group by previous use bins
        j = 0

        for k1, g1 in g.groupby(pd.cut(g["previousUse"], bins=nbBinsUse)):

#                 if (len(g2.dropna()) > 2) or (len(g2.dropna()) > 1 and g2.dropna().iloc[0,0] != g2.dropna().iloc[1,0]) :
            if len(g1.dropna()) > 1:
                # Learn a distribution for each group
                # using chaospy library.
                self.distributions[i,j] = cp.SampleDist(g1.dropna().iloc[:,0])

                if np.isnan(self.distributions[i,j].sample(1)):
                    self.distributions[i,j] = cp.Uniform(g1.iloc[:,0].min(),g1.iloc[:,0].max())

            else:
                # Assume uniform distribution if there is not enough data
                self.distributions[i,j] = cp.Uniform(g.iloc[:,0].min(),g.iloc[:,0].max())

            # Number of samples used for fitting each distribution
            self.samplesNumber[i,j] = len(g1.dropna())
            j = j + 1

        i = i+1

    if verbose:
        # Print some information about the number of samples used for the distributions
        print("Minimum number of samples for a distribution: " + str(self.samplesNumber.min()))
        print("Maximum number of samples for a distribution: " + str(self.samplesNumber.max()))
        print("Average number of samples for a distribution: " + str(self.samplesNumber.mean()))

    # Save distributions
    if save:
        self.save_model(filepath)

    return self.distributions

MarkovChainModelWeather.fit = fit

## Definition of the function to show the resulting distributions of the Markov Chain model
def show_model(self, detailed = False):
    print('TODO !')

MarkovChainModelWeather.show_model = show_model

## Definition of the sampling function for the Bayesian model
def sample(self, exog_data, nb_samples, yearly_samples = True, start_date = None, end_date = None, init_value = None, negativeValues = False):
    """ Default behaviour: create yearly samples.
    Otherwise:
     - yearly_samples == True: create samples with dates from start_date to end_date, starting with init_value
    """

    if yearly_samples:
        index_range = pd.date_range(start  = '1/1/2019',
                                        end    = '1/1/2020',
                                        freq   = "H",
                                        closed = "left")
        # Initialise previous use value
        initialUse = self.initialUseDistrib.sample(nb_samples)
        # exog_data starts by first day of the year and lasts exactly one year
        if not (exog_data.index[0].timetuple().tm_yday == 1) \
            or not ( (exog_data.index[-1] - exog_data.index[0]) == timedelta(days=364, hours=23) ) :
            raise Exception("exog_data must be a time serie over exactly a year.")


    else:
        index_range = pd.date_range(start = start_date,
                                    end   = end_date,
                                    freq  = "H")
        # Initialise previous use value: defined by the user
        initialUse = init_value * np.ones(nb_samples)
        if not exog_data.index.equals(index_range):
            raise Exception("exog_data and index_range must have the same index.")

    ## Create the base dataFrame
    profiles = pd.DataFrame(index = index_range)

    ## Populate with Markov chain model
    # Create data in a table
    tab_profiles = np.empty([len(profiles),nb_samples], dtype=np.float64)
    initialized = False
    newUse = np.empty([nb_samples], dtype=np.float64)
    t = 0
    for index, time in profiles.iterrows(): # loop over the rows of the DataFrame

        # Find column for exog data
        def positionExog(x):
            pos = np.argmax(x < self.exogBins_bounds) - 1
            if pos == -1 and (x < self.exogBins_bounds[0]):
                return 0
            elif pos == -1 and (x >= self.exogBins_bounds[-1]):
                return self.nbBinsExog - 1
            elif pos == -1:
                print(x < self.exogBins_bounds[0])
                print(x >= self.exogBins_bounds[-1])
                print(x)
                raise Exception('Problem with position function.')
            else:
                return pos
        colExog = positionExog(exog_data.iloc[t,0])

        # first iteration: put the initial use value on first line
        if not initialized:
            tab_profiles[0,:] = initialUse
            previousUse = initialUse
            initialized = True
        else:
            # add electricity demand for all the profiles for this time, using samplig method

            # Find the corresponding column in the frequencies table
            def positionUse(x):
                pos = np.argmax(x < self.previousUse_bounds[colExog]) - 1
                if pos == -1 and (x < self.previousUse_bounds[colExog][0]):
                    return 0
                elif pos == -1 and (x >= self.previousUse_bounds[colExog][-1]):
                    return self.nbBinsUse - 1
                elif pos == -1:
                    print(x < self.previousUse_bounds[colExog][0])
                    print(x >= self.previousUse_bounds[colExog][-1])
                    print(x)
                    raise Exception('Problem with position function.')
                else:
                    return pos

            colUse = np.array([positionUse(use) for use in previousUse])

            # Use the cumulative frequency function to derive the use value
            # Obtain the proper distributions for each profile
            distribs = self.distributions[colExog, colUse]
            # Apply each distribution to each seed value
            for i, f in enumerate(distribs):
                value = f.sample(1)

                if not negativeValues:
                    k = 0 # to prevent infinite loop
                    while value < 0 and k < 100: #loop until obtaining a positive value
                        value = f.sample(1)
                        k = k + 1
                    if k ==  100: raise Exception("It is impossible to have a non negatve value!")

                tab_profiles[t,i] = value
                if np.isnan(value):
                    print(f)
                    print(time.month-1)
                    print(time.hourOfDay)
                    print(col[i])

            # Update the previousUse
            previousUse = tab_profiles[t,:]

        t = t+1

    ## Create new columns
    for p in np.arange(nb_samples):
        profiles['sample'+str(p)] = tab_profiles[:,p]

    return profiles

MarkovChainModelWeather.sample = sample