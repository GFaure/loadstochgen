from .GenericStochasticModel import *

# General purpose
import pandas as pd
import pickle as pk
import numpy as np
from datetime import timedelta

# To do the decomposition
from sklearn import linear_model # extract linear trend
from pandas.core.nanops import nanmean as pd_nanmean # mean seasonal component
from pandas.tseries.offsets import MonthEnd
from pandas.tseries.frequencies import to_offset

# Models for remainders
from .STLbasedModelRemainder import *
from .MarkovChainModel import *
from .BayesianModel import *

# Plotting
import matplotlib.pyplot as plt
#import matplotlib.pylab as pl

class STLbasedModel(GenericStochasticModel):

    def __init__(self, raw_data = pd.DataFrame(), model = "additive"):
        """
        param model (optional, str {"additive", "multiplicative"}): Type of seasonal components.
        """

        GenericStochasticModel.__init__(self, raw_data)
        if model in ["additive", "multiplicative"]:
            self.model = model
        else:
            raise Exception(f'Value of parameter "model" must be "additive" or "multiplicative".')
        # Create the empty dataframes for the model
        self.T = None
        self.S = []
        self.R = pd.DataFrame()
        self.RM = None

        self.season_lengths = None
        self.season_granularities = None
        self.remainder_model = None

    def check_model(self):
        """
        reconstruct the initial data set to check the model
        """
        S = reconstruct_all_seasons(self, self.timestep, self.data.index)
        all_seasons = pd.DataFrame({predict_trend(self, self.data.index).columns[0] : S.sum(axis=1)})
        model = all_seasons + predict_trend(self, self.data.index) + self.R

        error = (model - self.data)**2
        print(error.describe())
        error.boxplot()
        return error

    def save_model(self, filepath):
        with open(filepath, "wb") as fp:
            pk.dump([self.T, self.S, self.R, self.RM, self.season_lengths, self.season_granularities, self.remainder_model], fp)

    def load_model(self, filepath):
        with open(filepath, "rb") as fp:
            self.T, self.S, self.R, self.RM, self.season_lengths, self.season_granularities, self.remainder_model = pk.load(fp)

## Function preprocess for STL model
def preprocess(self, raw_data = None):
    GenericStochasticModel.preprocess(self, raw_data)

    if self.model == "multiplicative":
        if self.data.min()[0] > 0:
            self.added_based = 0
        elif self.data.min()[0] == 0:
            # smaller value in the dataset strictly greater than 0
            self.added_based = self.data.loc[self.data.iloc[:,0] > 0].min()[0]
        else:
            raise Exception('Multiplicative model does not work with negative values for the moment.')

        ## TODO: voir pour ajouter une colonne plutot que d'ecraser
        ##       probleme avec fonction "decompose"...
        self.data.loc[self.data.iloc[:,0] == 0] = self.added_based
        self.data.iloc[:,0] = np.log(self.data.iloc[:,0])

    return self.data

STLbasedModel.preprocess = preprocess

def assess(self):
    trend = predict_trend(self, self.data.index)

    if self.model == "multiplicative":
        # Trend strengh
        FT_temp = 1 - np.exp(self.R).var()/np.exp(self.R+trend).var()
        FT = max(0 , FT_temp[0])
        # Seasonality strengh
        whole_seasons = reconstruct_all_seasons(self, self.timestep, self.data.index)
        FS_temp = 1 - (np.exp(whole_seasons.add(self.R, axis='index'))).var().rdiv(np.exp(self.R).var()[0], axis='index')
        FS = [ max(0 , x) for x in FS_temp ]

    else:
        # Trend strengh
        FT_temp = 1 - self.R.var()/(self.R+trend).var()
        FT = max(0 , FT_temp[0])
        # Seasonality strengh
        whole_seasons = reconstruct_all_seasons(self, self.timestep, self.data.index)
        FS_temp = 1 - (whole_seasons.add(self.R, axis='index')).var().rdiv(self.R.var()[0], axis='index')
        FS = [ max(0 , x) for x in FS_temp ]

    return FT, FS

STLbasedModel.assess = assess

def fit_trend(self, data = None):

    if data is None:
        data = self.data

    # Extract array from DataFrame
    y = data.iloc[:,0].values

    X = pd.to_numeric(data.index, downcast='float') / (3600 * 24 * 365)
    X = X[:,np.newaxis]

    # Keep only valid values for y
    mask = (y!=0) & (~np.isnan(y))

    # Fit line using all data
    self.T = linear_model.LinearRegression()
    self.T.fit(X[mask], y[mask])

    # Predict data of estimated models
    #line_X =    #np.arange(X.min(), X.max()+1)[:, np.newaxis]
    trend_ar = self.T.predict(X)

    # Create Dataframes
    trend = pd.DataFrame(index = data.index, data = { data.columns[0] : trend_ar })
    detrend = data - trend

    return detrend

def predict_trend(self, index_range):

    """ WARNING: index_range as to be a DateTimeIndex """

    X = pd.to_numeric(index_range, downcast='float') / (3600 * 24 * 365)
    X = X[:,np.newaxis]

    # Predict data from estimated model
    trend_ar = self.T.predict(X)

    # Create Dataframe
    trend = pd.DataFrame(index = index_range, data = { self.data.columns[0] : trend_ar })

    return trend

STLbasedModel.predict_trend = predict_trend

def fit_season(data, period):

    # Extract array from DataFrame
    y = data.iloc[:,0].values

    # period must not be larger than size of series to avoid introducing NaNs
    period = min(period, len(data))

    # calc one-period seasonality
    season_ar = np.array([pd_nanmean(y[i::period]) for i in range(period)])

    # 0-center the period avgs
    #season_ar -= np.mean(season_ar)

    # Create the dataframes
    season = pd.DataFrame(data = season_ar,
                          index = data.index[0:period],
                          columns = [data.columns[0]])

    return season

def reconstruct_whole_season(season, granularity, period, whole_timestep, whole_index):

    # Resample to initial timestep
    offset = ( (season.index[1] - season.index[0])/2 ).round(whole_timestep)
    upsampled_season = season.resample(rule = whole_timestep,
                                       loffset = offset).asfreq()
    # Add a granular element to complete the season
    if "M" in granularity:
        #This is a monthly granualrity and require specific trreatment
        offset_granular = pd.DateOffset(months = int(granularity[0:Mposition]) if granularity.find('M') > 0 else 1)
    else:
        offset_granular = to_offset(freq = granularity)
    one_granular = pd.DataFrame(index = pd.date_range(start = upsampled_season.index[-1] + whole_timestep,
                                                      end   = upsampled_season.index[-1] + offset_granular - whole_timestep,
                                                      freq  = whole_timestep),
                                columns  = upsampled_season.columns)
    upsampled_season = upsampled_season.append(one_granular)

    # Duplicate to take account of peridocity when interpolating
    upsampled_season.append(upsampled_season)
    upsampled_season.append(upsampled_season)
    # Interpolate
    upsampled_season = upsampled_season.interpolate(method='linear')
    # Remove additionnal rows
    upsampled_season = upsampled_season.dropna()
    upsampled_season = upsampled_season.loc[~upsampled_season.index.duplicated(keep='first')]
    upsampled_season = upsampled_season.sort_index()

    # Construct whole season Dataframe : start index
    start_index = upsampled_season.index[0] - len(upsampled_season)*whole_timestep
    # Adjustment of year to be sure that we start just before the beginning of whole_timestep
    start_index = start_index.replace(year = whole_index[0].year)
    if start_index > whole_index[0]:
        start_index = start_index.replace(year = whole_index[0].year-1)

    # Construct whole season Dataframe : data
    nb_tiles = int( ((whole_index[-1] - start_index).total_seconds() // whole_timestep.total_seconds() + 1) // len(upsampled_season) )
    data_whole_season = np.tile(upsampled_season.iloc[:,0], nb_tiles + 2)

    index_whole_season = pd.date_range(start = start_index,
                                       periods = data_whole_season.shape[0],
                                       freq = whole_timestep)

    whole_season = pd.DataFrame(index = index_whole_season,
                                data = data_whole_season,
                                columns = [season.columns[0]])

    whole_season = whole_season.loc[whole_index,:]

    return whole_season

def reconstruct_all_seasons(self, timestep, index):
    for idx, season in enumerate(self.S):
        if idx == 0:
            S = reconstruct_whole_season(season,
                                 self.season_granularities[idx],
                                 self.season_lengths[idx],
                                 timestep,
                                 index)
        else:
            S = S.merge(reconstruct_whole_season(season,
                                 self.season_granularities[idx],
                                 self.season_lengths[idx],
                                 timestep,
                                 index),
                    left_index = True,
                    right_index = True,
                    suffixes = ('','_' + str(self.season_lengths[idx]) + self.season_granularities[idx]))
    S = S.rename(columns = {S.columns[0] : S.columns[0] + '_' + str(self.season_lengths[0]) + self.season_granularities[0]})

    return S

def decompose(self, season_lengths, season_granularities, preprocessed_data = None, verbose = True, save = False, filepath = ''):
    """
    param:
    season_lengths (int numpy array): list of the number of time steps corresponding to the length of each season.
    season_granularities (str {'H', 'D', 'MS'} numpy array): granularity required for each season (time serie will be agreggated to
                                                            this granularity before seasonal component extraction). Same keywords as
                                                            for pandas.DataFrame.resample
    """
    if len(season_lengths) != len(season_granularities):
        raise Exception('season_lengths and season_granularities should have the same length.')

    if preprocessed_data is None:
        preprocessed_data = self.data

    self.season_lengths = season_lengths
    self.season_granularities = season_granularities
    # TODO: Sort season_lengths and season_granularities in ascending season lengths
    #nbTimeStep[self.season_granularities == 'H'] = 1
    #nbTimeStep[self.season_granularities == 'D'] = 24
    #nbTimeStep[self.season_granularities == 'M'] = 24*30
    #sort_indexes = np.argsort(season_lengths * nbTimeStep)
    #self.season_lengths = season_lengths[sort_indexes]
    #self.season_granularities = season_granularities[sort_indexes]

    # Store initial timestep
    self.timestep = preprocessed_data.index[1] - preprocessed_data.index[0]

    # Extract and store trend component
    deseasonalized_data = fit_trend(self, preprocessed_data)

    # Extract seasonal components from the shortest to the longest
    self.S = []
    for idx, season_length in enumerate(self.season_lengths):
        # Aggregate time serie to the season_granularity (sum)
        ## TODO: authorized other ways to aggregate (sum, last value)
        resampled_data = deseasonalized_data.resample(self.season_granularities[idx],
                                                           convention="start").mean()

        # Fit the season decomposition
        season = fit_season(resampled_data,
                            period = season_length)
        # Store the seasonal component as new dataframe
        self.S.append(season)

        # Reconstruct the seasonal component in original granularity
        tempS = reconstruct_whole_season(season,
                                         self.season_granularities[idx],
                                         season_length,
                                         self.timestep,
                                         preprocessed_data.index)
        # Remaining signal
        deseasonalized_data -= tempS

    # Store the remainder component
    self.R = deseasonalized_data

    if verbose:
        # Print the strength of trend and seasonality
        # TODO update seasonal components calculation
        FT, FS = self.assess()
        print('FT: ' + str(FT*100) + ' %')
        for idx, season_length in enumerate(self.season_lengths):
            print('FS for '+ str(season_length) + ' ' + str(self.season_granularities[idx]) + ': ' + str(FS[idx]*100) + ' %')

    # Save distributions
    if save:
        self.save_model(filepath)

    return self.T, self.S, self.R

STLbasedModel.decompose = decompose

def fit_remainder(self, model, verbose = True, save = False, filepath = ''):
    """
    param:
    model (str {'H', 'D', 'MS'} numpy array): model used to model the remainder component. List of implemented models:
        "SD": one single distribution
        "SMC": a simple Markov Chain
        "BN": a Bayesian Network
        "MC": a Markov Chain
    """
    if model == "SD":
        self.RM = Rem_SimpleDistrib(self.R)
    elif model == "SMC":
        self.RM = Rem_SimpleMC(self.R)
    elif model == "MC":
        self.RM = MarkovChainModel(self.R)
    elif model == "BN":
        self.RM = BayesianModel(self.R)
    else:
        raise Exception("This model is not recognized nor implemented.")

    self.RM.preprocess()
    self.RM.fit(verbose = verbose, save = save)

    self.remainder_model = model

    # Save model
    if save:
        self.save_model(filepath)

    return self.RM

STLbasedModel.fit_remainder = fit_remainder

def fit(self, season_lengths, season_granularities, model, preprocessed_data = None, verbose = True, save = False, filepath = ''):

    self.decompose(season_lengths, season_granularities, preprocessed_data, verbose)

    # 2nd iteration of decomposition
    T1 = self.predict_trend(self.data.index)
    S1 = self.S
    self.decompose(season_lengths, season_granularities, self.R, verbose)
    # join the results
    fit_trend(self, T1 + self.predict_trend(self.data.index) ) # refit the trend
    self.S = [ S1[i] + self.S[i] for i in range(len(self.S)) ]

    # Fit the remainder
    self.fit_remainder(model, verbose = verbose, save = save)

    # Save model
    if save:
        self.save_model(filepath)

    return self.T, self.S, self.RM

STLbasedModel.fit = fit

## Definition of the function to show the resulting distributions of the Bayesian model
def show_model(self, detailed = False):

    S = reconstruct_all_seasons(self, self.timestep, self.data.index)

    if self.model == "additive":
        data = self.data
        T = predict_trend(self, self.data.index)
        R = self.R
    elif self.model == "multiplicative":
        data = self.raw_data
        T = np.exp(predict_trend(self, self.data.index))
        S = np.exp(S)
        R = np.exp(self.R)
    else:
        raise Exception(f'Value of parameter "model" must be "additive" or "multiplicative".')

    # Global plot
    fig, ax = plt.subplots(1, 1, figsize=(24, 5))
    data.plot(ax=ax, style='k', label='initial');
    R.plot(ax=ax, style='gray', label='remainder');
    S.plot(ax=ax, colormap='winter', label='seasonal');
    T.plot(ax=ax, style='r', label='trend');
    ax.legend();
    ax.set_ylabel('use (kW)')
    #ax.set_xlim(['2014-01-01','2015-01-01']);
    # Plot of the different components
    fig, ax = plt.subplots(4, 1, figsize=(24, 4*4))
    #xlim = ['2014-01-01','2015-01-01']
    data.plot(ax=ax[0], style='k', label='initial');
    ax[0].legend();
    ax[0].set_ylabel('use (kW)')
    #ax[0].set_xlim(xlim);
    T.plot(ax=ax[1], style='r', label='trend');
    ax[1].legend();
    ax[1].set_ylabel('use (kW)')
    #ax[1].set_xlim(xlim);
    S.plot(ax=ax[2], colormap='winter');
    ax[2].legend();
    ax[2].set_ylabel('use (kW)')
    #ax[2].set_xlim(xlim);
    R.plot(ax=ax[3], style='gray', label='remainder');
    ax[3].legend();
    ax[3].set_ylabel('use (kW)')
    #ax[3].set_xlim(xlim);

STLbasedModel.show_model = show_model

## Definition of the sampling function for the seasonal decomposition-based model
def sample(self, nb_samples, yearly_samples = True, start_date = None, end_date = None, init_value = None,
           negativeValues = False, fixed_trend_start = False, trend_range_start_date = None, trend_range_end_date = None):

    """ Default behaviour: create yearly samples, with random start value for the trend picked up from the initial data set.
    Otherwise:
     - yearly_samples == True: create samples with dates from start_date to end_date
     - fixed_trend_start == True: trend timestamps are fixed to [start_date:end_date]
     - trend_range_start_date and trend_range_end_date: can be used to propose another range for random pick-up of the trend.

    """

    # Sample the remainder model
    profiles = self.RM.sample(nb_samples, yearly_samples, start_date, end_date, init_value, negativeValues = True)

    # Compute the sum of all seasons
    S = reconstruct_all_seasons(self, self.timestep, profiles.index)
    # Select trend
    if fixed_trend_start:
        index_range_trend = [profiles.index for i in np.arange(nb_samples)]
    else: # random start
        if trend_range_start_date is not None:

            if trend_range_end_date is None:
                raise Exception("If you specify a start date for trend range, you should also specify an end date.")

            trend_range = pd.date_range(start = trend_range_start_date,
                                        end   = trend_range_end_date,
                                        freq  = "H",
                                        closed ="left")
        else:
            trend_range = self.data.index

        if len(trend_range) < len(S):
            raise Exception("Length of trend range must be greater or equal than samples length.")

        start_indexes = np.random.randint( len(trend_range)-len(S), size = nb_samples )
        index_range_trend = [trend_range[start_indexes[i]:start_indexes[i]+len(S)] for i in np.arange(nb_samples)]

    # Create trend Dataframes and convert them into a 8760 x nb_samples array for the whole Dataframe creation
    trend_data = np.stack([predict_trend(self, index_range_trend[i]).to_numpy() for i in np.arange(nb_samples)] )
    trend_data = np.squeeze(trend_data).transpose()

    trend = pd.DataFrame(index   = S.index,
                         columns = ['sample'+str(p) for p in range(nb_samples)],
                         data    = trend_data )

    # Add seasons and trend to remainder samples
    profiles = profiles + trend
    profiles = profiles.add(S.iloc[:,0], axis = "index")

    if self.model == 'multiplicative':
        # take exponential of the results
        profiles = np.exp(profiles)

    if not negativeValues: # 0 instead of negative values
        profiles[profiles < 0] = 0

    return profiles

STLbasedModel.sample = sample