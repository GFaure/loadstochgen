# loadstochgen

This library proposes several stochastic generators specifically designed for energy load time series at hourly time step, which are characterized by strong seasonnalities and a high autocorrelation.