from loadstochgen.models import BayesianModel
import pandas as pd

## All the models heritates from GenericStochasticModel which defines a common API interface (i.e. a common set of
## functions to play with).


print("Load energy use data from a building...")
buildingUse = pd.read_csv("Residential_19.csv", parse_dates = {'time' : ['date', 'hour']} )
# Set column "time" as index of the DataFrame
buildingUse = buildingUse.set_index('time')

print(buildingUse.head())

print("From these raw data, we want to learn a Bayesian network.")

# Creation of the new instance
myModel = BayesianModel(buildingUse)
# Pre-process of the data
myModel.preprocess()

# Show the first lines of the preprocessed DataFrame
print("After the creation of a new instance and pre-process of the data. They looks like this:")
print(myModel.data.head())

# Fit the model on the data
print("Let's fit the model!")
myModel.fit()

# Sample from the model
print("Perfect! You can now create as many random samples as you like.\n By default, the samples represent one year of data.")
all_samples = myModel.sample(nb_samples = 3)
print(all_samples)

# Save the model
print("You can now save the model for later.")
myModel.save("myBayesianModel.dat")